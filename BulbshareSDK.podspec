Pod::Spec.new do |spec|
    spec.name         = 'BulbshsareSDK'
    spec.version      = '0.1.0'
    spec.license      = "MIT"
    spec.homepage     = 'https://sanchitsrivastava247@bitbucket.org/sanchitsrivastava247/sdk'
    spec.authors      = { 'sanchit srivastava' => 'sanchit.srivastava@daffodilsw.com' }
    spec.summary      = 'Blub share SDK Cocoa Pod'
    spec.description  = 'Buld share SDK Cocoa Pod'
    spec.source       = { :git => 'https://sanchitsrivastava247@bitbucket.org/sanchitsrivastava247/sdk.git', :tag => spec.version }
    spec.vendored_frameworks = "BulbshareSDK.xcframework"
    spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.platform = :ios
    spec.swift_version = "4.2"
    spec.ios.deployment_target  = '11.0'
  end